const hexToRgbaValue = (hex) => {
	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i
		.exec(hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, (m, r, g, b) => r + r + g + g + b + b))
	return result ?
		[parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16), 1]
			.map(Math.round) :
		null
}

const addAlphaChannel = rgb => rgb.length < 4 ? rgb.concat([1]) : rgb

const toRgbaValue = (str) =>
	addAlphaChannel(str.replace(/^rgb\(|^rgba\(/, '')
		.replace(')', '')
		.split(',')
    .map(Number)
		.map(Math.round))

const toColorValues = str => /^#/.test(str) ?
	hexToRgbaValue(str) : toRgbaValue(str)	

const valToHex = c => c.toString(16).length === 1 ?
	`0${c.toString(16)}` : c.toString(16)

const rgbToHex = ([r, g, b]) =>
	`#${valToHex(r)}${valToHex(g)}${valToHex(b)}`

const hexToRgb = (hex) => {
	const [r, g, b] = hexToRgbaValue(hex)
	return `rgb(${r},${g},${b})`
}

const hexToRgba = (hex) => {
	const [r, g, b] = hexToRgbaValue(hex)
	return `rgba(${r},${g},${b},1)`
}

const rgbToRgba = (rgb) => {
	const [r, g, b, a] = toRgbaValue(rgb)
	return `rgba(${r},${g},${b},1)`
}

const toRgbaFromValues = ([r, g, b, a]) =>
	`rgba(${Math.round(r)},${Math.round(g)},${Math.round(b)},${a})`

export { 
	hexToRgbaValue,
	toRgbaValue,
	toColorValues,
	rgbToHex,
	hexToRgb,
	hexToRgba,
	rgbToRgba,
	toRgbaFromValues
}
