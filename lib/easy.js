const FRAME_DURATION = 1000 / 60

const ease = {
	linear: (t, b, c, d) => c*t/d + b,
	inQuad: (t, b, c, d) => {
		t /= d
		return c*t*t + b
	},
	outQuad: (t, b, c, d) => {
		t /= d
		return -c * t*(t-2) + b
	},
	inOutQuad: (t, b, c, d) => {
		t /= d/2
		if (t < 1) return c/2*t*t + b
		t--
		return -c/2 * (t*(t-2) - 1) + b
	},
	inCubic: (t, b, c, d) => {
		t /= d
		return c*t*t*t + b
	},
	outCubic: (t, b, c, d) => {
		t /= d
		t--
		return c*(t*t*t + 1) + b
	},
	inOutCubic: (t, b, c, d) => {
		t /= d/2
		if (t < 1) return c/2*t*t*t + b
		t -= 2
		return c/2*(t*t*t + 2) + b
	},
	inQuart: (t, b, c, d) => {
		t /= d
		return c*t*t*t*t + b
	},
	outQuart: (t, b, c, d) => {
		t /= d
		t--
		return -c * (t*t*t*t - 1) + b
	},
	inOutQuart: (t, b, c, d) => {
		t /= d/2
		if (t < 1) return c/2*t*t*t*t + b
		t -= 2
		return -c/2 * (t*t*t*t - 2) + b
	},
	inQuint: (t, b, c, d) => {
		t /= d
		return c*t*t*t*t*t + b
	},
	outQuint: (t,b,c,d) => {
		t /= d
		t--
		return c*(t*t*t*t*t + 1) + b
	},
	inOutQuint: (t,b,c,d) => {
		t /= d/2
		if (t < 1) return c/2*t*t*t*t*t + b
		t -= 2
		return c/2*(t*t*t*t*t + 2) + b
	},
	inSine: (t,b,c,d) => -c * Math.cos(t/d * (Math.PI/2)) + c + b,
	outSine: (t,b,c,d) => c * Math.sin(t/d * (Math.PI/2)) + b,
	inOutSine: (t,b,c,d) => -c/2 * (Math.cos(Math.PI*t/d) - 1) + b,
	inExpo: (t,b,c,d) => c * Math.pow( 2, 10 * (t/d - 1) ) + b,
	outExpo: (t,b,c,d) => c * ( -Math.pow( 2, -10 * t/d ) + 1 ) + b,
	inOutExpo: (t,b,c,d) => {
		t /= d/2
		if (t < 1) return c/2 * Math.pow( 2, 10 * (t - 1) ) + b
		t--
		return c/2 * ( -Math.pow( 2, -10 * t) + 2 ) + b
	},
	inCirc: (t,b,c,d) => {
		t /= d
		return -c * (Math.sqrt(1 - t*t) - 1) + b
	},
	outCirc: (t,b,c,d) => {
		t /= d
		t--
		return c * Math.sqrt(1 - t*t) + b
	},
	inOutCirc: (t,b,c,d) => {
		t /= d/2
		if (t < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b
		t -= 2
		return c/2 * (Math.sqrt(1 - t*t) + 1) + b
	}
}

export default (type, b,c,d) => {
	let t = 0
	let output = []
	const tDelta = d - t
	for(let i = 0, l = Math.round(tDelta / FRAME_DURATION); i < l; i++) {
		t += FRAME_DURATION
		output.push(ease[type](t,b,c-b,d))		
	}
	output[output.length - 1] = c
	return output	
}
