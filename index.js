import easy from './lib/easy'
import { toRgbaFromValues, toColorValues } from './lib/color'

const applyValues = (obj, steps, curStep, fn) => {
  for(let key in steps) {
    if(steps.hasOwnProperty(key)) {
      if(typeof fn === 'function') {
        fn.apply(obj, steps[key])
      } else {
        obj[key] = steps[key][curStep]
      }
    }
  }
}

const transpose = m => m[0].map((x,i) => m.map(x => x[i]))

const log = x => {
  console.log(x)
  return x
}

const buildArraySteps = (origin, dur, destinations, easeType) =>
  transpose(
    destinations
      .map((dest, i) => easy(easeType, origin[i], dest, dur))
  )

const buildPropSteps = (origin, dur, destinations, easeType) =>
  Object.keys(destinations)
    .reduce((acc, prop) => {
      acc[prop] = easy(easeType, origin[prop], destinations[prop], dur)
      return acc
    }, {})

const buildColorSteps = (origColor, dur, destColor, easeType) => {
  const propColor = toColorValues(destColor)
  const [rs, gs, bs, as] = toColorValues(origColor)	
    .map((c, i) => easy(easeType, c, propColor[i], dur))
  return rs.map((r, i) => toRgbaFromValues([r, gs[i], bs[i], as[i]]))
}

const parseCssUnit = (value) => {
  const [_, n, unit] = value.match(/^([+-]?(?:\d+|\d*\.\d+))([a-z]*|%)$/)
  return {n, unit}
}

const buildUnitSteps = (origValue, dur, destValue, easeType) => {
  const t = parseCssUnit(origValue)
  const d = parseCssUnit(destValue)
  if(t.unit != d.unit) throw new Error(`Unit type mismatch from ${origValue} to ${destValue}`)
  return easy(easeType, t.n, d.n, dur).map(n => n + d.unit)
}

const buildSteps = (target, dur, props, easeType) =>
  (typeof props.fn === 'function' && props.to && props.from) ?
    buildArraySteps(props.from, dur, props.to, easeType) :
    Object.keys(props)
      .reduce((acc, key) => {
        if(/color/i.test(key)) {
          acc[key] = buildColorSteps(target[key], dur, props[key], easeType)
        } else if(/px|em|rem|pt|%/g.test(target[key])) {
          acc[key] = buildUnitSteps(target[key], dur, props[key], easeType)
        } else {
          acc[key] = easy(easeType, target[key], props[key], dur)
        }
        return acc
      }, {})

const createRevStepper = (inst, steps, cb) => {
	return function revStepper() {
		let [ prop ] = Object.keys(steps)
		if(steps[prop][inst.curStep] === undefined) {
			inst.curStep = 0
      cancelAnimationFrame(inst.rAF)
			if(typeof cb === 'function') {
				cb.apply(null, [inst])
			}
		} else {
			applyValues(inst.target, steps, inst.curStep)
			inst.curStep --
			inst.rAF = requestAnimationFrame(revStepper)
		}
	}
}

const createStepper = (inst, steps, cb) => {
	return function stepper() {
		let [ prop ] = Object.keys(steps)
		if(steps[prop][inst.curStep] === undefined) {
			inst.curStep = inst.steps[prop].length - 1
      cancelAnimationFrame(inst.rAF)
			if(typeof cb === 'function') {
				cb.apply(null, [inst])
			}
		} else {
			applyValues(inst.target, steps, inst.curStep, inst.toProps.fn)
			inst.curStep ++
			inst.rAF = requestAnimationFrame(stepper)
		}
	}
}

class Anim {
  constructor(target, dur, props) {
    const toProps = {}
    for(let key in props) {
      if(props.hasOwnProperty(key)) {
        if(!/ease|onStart|onUpdate|onComplete/.test(key)) {
          toProps[key] = props[key]
        }
      }
    }
    this.startTime = window.performance.now ?
      (performance.now() + performance.timing.navigationStart) :
      Date.now()
    this.target = target instanceof HTMLElement ? target.style : target
    this.onUpdate = props.onUpdate
    this.toProps = toProps
    this.duration = dur * 1000
    this.steps = buildSteps(this.target, this.duration, toProps, props.ease || 'inOutQuad')
		this.curStep = 0
    if(typeof props.onStart === 'function') props.onStart.call(this)
  }
	rewind(cb) {
    cancelAnimationFrame(this.rAF)
    const _steps = JSON.parse(JSON.stringify(this.steps))
		this.rAF = requestAnimationFrame(createRevStepper(this, _steps, cb))
    return this
	}
  play(cb) {
    cancelAnimationFrame(this.rAF)
    const _steps = JSON.parse(JSON.stringify(this.steps))
		this.rAF = requestAnimationFrame(createStepper(this, _steps, cb))
    return this
  }
	playReset(cb) {
		this.play((inst) => {
			inst.curStep = 0
			if(typeof cb === 'function') cb(inst)
		})
    return this
	}
	stop() {
		cancelAnimationFrame(this.rAF)
    return this
	}
}

const anim = (target, dur, props) => new Anim(target, dur, props)

anim.to = (target, dur, props) => {
  const inst = new Anim(target, dur, props)
  inst.play(props.onComplete)
  return inst
}

anim.buildArraySteps = buildArraySteps

export default anim
