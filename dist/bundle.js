var anim = (function () {
'use strict';

const FRAME_DURATION = 1000 / 60;

const ease = {
	linear: (t, b, c, d) => c*t/d + b,
	inQuad: (t, b, c, d) => {
		t /= d;
		return c*t*t + b
	},
	outQuad: (t, b, c, d) => {
		t /= d;
		return -c * t*(t-2) + b
	},
	inOutQuad: (t, b, c, d) => {
		t /= d/2;
		if (t < 1) return c/2*t*t + b
		t--;
		return -c/2 * (t*(t-2) - 1) + b
	},
	inCubic: (t, b, c, d) => {
		t /= d;
		return c*t*t*t + b
	},
	outCubic: (t, b, c, d) => {
		t /= d;
		t--;
		return c*(t*t*t + 1) + b
	},
	inOutCubic: (t, b, c, d) => {
		t /= d/2;
		if (t < 1) return c/2*t*t*t + b
		t -= 2;
		return c/2*(t*t*t + 2) + b
	},
	inQuart: (t, b, c, d) => {
		t /= d;
		return c*t*t*t*t + b
	},
	outQuart: (t, b, c, d) => {
		t /= d;
		t--;
		return -c * (t*t*t*t - 1) + b
	},
	inOutQuart: (t, b, c, d) => {
		t /= d/2;
		if (t < 1) return c/2*t*t*t*t + b
		t -= 2;
		return -c/2 * (t*t*t*t - 2) + b
	},
	inQuint: (t, b, c, d) => {
		t /= d;
		return c*t*t*t*t*t + b
	},
	outQuint: (t,b,c,d) => {
		t /= d;
		t--;
		return c*(t*t*t*t*t + 1) + b
	},
	inOutQuint: (t,b,c,d) => {
		t /= d/2;
		if (t < 1) return c/2*t*t*t*t*t + b
		t -= 2;
		return c/2*(t*t*t*t*t + 2) + b
	},
	inSine: (t,b,c,d) => -c * Math.cos(t/d * (Math.PI/2)) + c + b,
	outSine: (t,b,c,d) => c * Math.sin(t/d * (Math.PI/2)) + b,
	inOutSine: (t,b,c,d) => -c/2 * (Math.cos(Math.PI*t/d) - 1) + b,
	inExpo: (t,b,c,d) => c * Math.pow( 2, 10 * (t/d - 1) ) + b,
	outExpo: (t,b,c,d) => c * ( -Math.pow( 2, -10 * t/d ) + 1 ) + b,
	inOutExpo: (t,b,c,d) => {
		t /= d/2;
		if (t < 1) return c/2 * Math.pow( 2, 10 * (t - 1) ) + b
		t--;
		return c/2 * ( -Math.pow( 2, -10 * t) + 2 ) + b
	},
	inCirc: (t,b,c,d) => {
		t /= d;
		return -c * (Math.sqrt(1 - t*t) - 1) + b
	},
	outCirc: (t,b,c,d) => {
		t /= d;
		t--;
		return c * Math.sqrt(1 - t*t) + b
	},
	inOutCirc: (t,b,c,d) => {
		t /= d/2;
		if (t < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b
		t -= 2;
		return c/2 * (Math.sqrt(1 - t*t) + 1) + b
	}
};

var easy = (type, b,c,d) => {
	let t = 0;
	let output = [];
	const tDelta = d - t;
	for(let i = 0, l = Math.round(tDelta / FRAME_DURATION); i < l; i++) {
		t += FRAME_DURATION;
		output.push(ease[type](t,b,c-b,d));		
	}
	output[output.length - 1] = c;
	return output	
};

const hexToRgbaValue = (hex) => {
	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i
		.exec(hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, (m, r, g, b) => r + r + g + g + b + b));
	return result ?
		[parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16), 1]
			.map(Math.round) :
		null
};

const addAlphaChannel = rgb => rgb.length < 4 ? rgb.concat([1]) : rgb;

const toRgbaValue = (str) =>
	addAlphaChannel(str.replace(/^rgb\(|^rgba\(/, '')
		.replace(')', '')
		.split(',')
    .map(Number)
		.map(Math.round));

const toColorValues = str => /^#/.test(str) ?
	hexToRgbaValue(str) : toRgbaValue(str);	

const toRgbaFromValues = ([r, g, b, a]) =>
	`rgba(${Math.round(r)},${Math.round(g)},${Math.round(b)},${a})`;

const applyValues = (obj, steps, curStep, fn) => {
  for(let key in steps) {
    if(steps.hasOwnProperty(key)) {
      if(typeof fn === 'function') {
        fn.apply(obj, steps[key]);
      } else {
        obj[key] = steps[key][curStep];
      }
    }
  }
};

const transpose = m => m[0].map((x,i) => m.map(x => x[i]));

const buildArraySteps = (origin, dur, destinations, easeType) =>
  transpose(
    destinations
      .map((dest, i) => easy(easeType, origin[i], dest, dur))
  );

const buildColorSteps = (origColor, dur, destColor, easeType) => {
  const propColor = toColorValues(destColor);
  const [rs, gs, bs, as] = toColorValues(origColor)	
    .map((c, i) => easy(easeType, c, propColor[i], dur));
  return rs.map((r, i) => toRgbaFromValues([r, gs[i], bs[i], as[i]]))
};

const parseCssUnit = (value) => {
  const [_, n, unit] = value.match(/^([+-]?(?:\d+|\d*\.\d+))([a-z]*|%)$/);
  return {n, unit}
};

const buildUnitSteps = (origValue, dur, destValue, easeType) => {
  const t = parseCssUnit(origValue);
  const d = parseCssUnit(destValue);
  if(t.unit != d.unit) throw new Error(`Unit type mismatch from ${origValue} to ${destValue}`)
  return easy(easeType, t.n, d.n, dur).map(n => n + d.unit)
};

const buildSteps = (target, dur, props, easeType) =>
  (typeof props.fn === 'function' && props.to && props.from) ?
    buildArraySteps(props.from, dur, props.to, easeType) :
    Object.keys(props)
      .reduce((acc, key) => {
        if(/color/i.test(key)) {
          acc[key] = buildColorSteps(target[key], dur, props[key], easeType);
        } else if(/px|em|rem|pt|%/g.test(target[key])) {
          acc[key] = buildUnitSteps(target[key], dur, props[key], easeType);
        } else {
          acc[key] = easy(easeType, target[key], props[key], dur);
        }
        return acc
      }, {});

const createRevStepper = (inst, steps, cb) => {
	return function revStepper() {
		let [ prop ] = Object.keys(steps);
		if(steps[prop][inst.curStep] === undefined) {
			inst.curStep = 0;
      cancelAnimationFrame(inst.rAF);
			if(typeof cb === 'function') {
				cb.apply(null, [inst]);
			}
		} else {
			applyValues(inst.target, steps, inst.curStep);
			inst.curStep --;
			inst.rAF = requestAnimationFrame(revStepper);
		}
	}
};

const createStepper = (inst, steps, cb) => {
	return function stepper() {
		let [ prop ] = Object.keys(steps);
		if(steps[prop][inst.curStep] === undefined) {
			inst.curStep = inst.steps[prop].length - 1;
      cancelAnimationFrame(inst.rAF);
			if(typeof cb === 'function') {
				cb.apply(null, [inst]);
			}
		} else {
			applyValues(inst.target, steps, inst.curStep, inst.toProps.fn);
			inst.curStep ++;
			inst.rAF = requestAnimationFrame(stepper);
		}
	}
};

class Anim {
  constructor(target, dur, props) {
    const toProps = {};
    for(let key in props) {
      if(props.hasOwnProperty(key)) {
        if(!/ease|onStart|onUpdate|onComplete/.test(key)) {
          toProps[key] = props[key];
        }
      }
    }
    this.startTime = window.performance.now ?
      (performance.now() + performance.timing.navigationStart) :
      Date.now();
    this.target = target instanceof HTMLElement ? target.style : target;
    this.onUpdate = props.onUpdate;
    this.toProps = toProps;
    this.duration = dur * 1000;
    this.steps = buildSteps(this.target, this.duration, toProps, props.ease || 'inOutQuad');
		this.curStep = 0;
    if(typeof props.onStart === 'function') props.onStart.call(this);
  }
	rewind(cb) {
    cancelAnimationFrame(this.rAF);
    const _steps = JSON.parse(JSON.stringify(this.steps));
		this.rAF = requestAnimationFrame(createRevStepper(this, _steps, cb));
    return this
	}
  play(cb) {
    cancelAnimationFrame(this.rAF);
    const _steps = JSON.parse(JSON.stringify(this.steps));
		this.rAF = requestAnimationFrame(createStepper(this, _steps, cb));
    return this
  }
	playReset(cb) {
		this.play((inst) => {
			inst.curStep = 0;
			if(typeof cb === 'function') cb(inst);
		});
    return this
	}
	stop() {
		cancelAnimationFrame(this.rAF);
    return this
	}
}

const anim = (target, dur, props) => new Anim(target, dur, props);

anim.to = (target, dur, props) => {
  const inst = new Anim(target, dur, props);
  inst.play(props.onComplete);
  return inst
};

anim.buildArraySteps = buildArraySteps;

return anim;

}());
