;(function(global) {
  /////////////////////
  // EASING FORMULAS //
  /////////////////////
  var FRAME_DURATION = 1000 / 60

  var ease = {
    linear: function(t, b, c, d) { return c*t/d + b },
    inQuad: function(t, b, c, d) {
      t /= d
      return c*t*t + b
    },
    outQuad: function(t, b, c, d) {
      t /= d
      return -c * t*(t-2) + b
    },
    inOutQuad: function(t, b, c, d) {
      t /= d/2
      if (t < 1) return c/2*t*t + b
      t--
      return -c/2 * (t*(t-2) - 1) + b
    },
    inCubic: function(t, b, c, d) {
      t /= d
      return c*t*t*t + b
    },
    outCubic: function(t, b, c, d) {
      t /= d
      t--
      return c*(t*t*t + 1) + b
    },
    inOutCubic: function(t, b, c, d) {
      t /= d/2
      if (t < 1) return c/2*t*t*t + b
      t -= 2
      return c/2*(t*t*t + 2) + b
    },
    inQuart: function(t, b, c, d) {
      t /= d
      return c*t*t*t*t + b
    },
    outQuart: function(t, b, c, d) {
      t /= d
      t--
      return -c * (t*t*t*t - 1) + b
    },
    inOutQuart: function(t, b, c, d) {
      t /= d/2
      if (t < 1) return c/2*t*t*t*t + b
      t -= 2
      return -c/2 * (t*t*t*t - 2) + b
    },
    inQuint: function(t, b, c, d) {
      t /= d
      return c*t*t*t*t*t + b
    },
    outQuint: function(t,b,c,d) {
      t /= d
      t--
      return c*(t*t*t*t*t + 1) + b
    },
    inOutQuint: function(t,b,c,d) {
      t /= d/2
      if (t < 1) return c/2*t*t*t*t*t + b
      t -= 2
      return c/2*(t*t*t*t*t + 2) + b
    },
    inSine: function(t,b,c,d) { return -c * Math.cos(t/d * (Math.PI/2)) + c + b },
    outSine: function(t,b,c,d) { return c * Math.sin(t/d * (Math.PI/2)) + b },
    inOutSine: function(t,b,c,d) { return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b },
    inExpo: function(t,b,c,d) { return c * Math.pow( 2, 10 * (t/d - 1) ) + b },
    outExpo: function(t,b,c,d) { return c * ( -Math.pow( 2, -10 * t/d ) + 1 ) + b },
    inOutExpo: function(t,b,c,d) {
      t /= d/2
      if (t < 1) return c/2 * Math.pow( 2, 10 * (t - 1) ) + b
      t--
      return c/2 * ( -Math.pow( 2, -10 * t) + 2 ) + b
    },
    inCirc: function(t,b,c,d) {
      t /= d
      return -c * (Math.sqrt(1 - t*t) - 1) + b
    },
    outCirc: function(t,b,c,d) {
      t /= d
      t--
      return c * Math.sqrt(1 - t*t) + b
    },
    inOutCirc: function(t,b,c,d) {
      t /= d/2
      if (t < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b
      t -= 2
      return c/2 * (Math.sqrt(1 - t*t) + 1) + b
    }
  }

  function easy(type, b,c,d) {
    var t = 0
    var output = []
    var tDelta = d - t
    for(var i = 0, l = Math.round(tDelta / FRAME_DURATION); i < l; i++) {
      t += FRAME_DURATION
      output.push(ease[type](t,b,c-b,d))		
    }
    output[output.length - 1] = c
    return output	
  }



  /////////////////
  // COLOR UTILS //
  /////////////////
  function hexToRgbaValue(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i
      .exec(hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function(m, r, g, b) {
        return r + r + g + g + b + b
      }))
    return result ?
      [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16), 1]
        .map(Math.round) :
      null
  }

  function addAlphaChannel(rgb) {
    return rgb.length < 4 ? rgb.concat([1]) : rgb
  }

  function toRgbaValue(str) {
    return addAlphaChannel(
      str.replace(/^rgb\(|^rgba\(/, '')
      .replace(')', '')
      .split(',')
      .map(Number)
      .map(Math.round)
    )
  }

  function toColorValues(str) {
    return /^#/.test(str) ?
      hexToRgbaValue(str) :
      toRgbaValue(str)
  }

  function valToHex(c) {
    return c.toString(16).length === 1 ?
    '0' + c.toString(16) :
    c.toString(16)
  } 

  function rgbToHex(rgb) {
    return '#' + valToHex(rgb[0]) + valToHex(rgb[1]) + valToHex(rgb[2])
  }

  function hexToRgb(hex) {
    var rgb = hexToRgbaValue(hex)
    return 'rgb(' + rgb[0] +  ',' + rgb[1] + ',' + rgb[2] + ')'
  }

  function hexToRgba(hex) {
    var rgb = hexToRgbaValue(hex)
    return 'rgba(' + rgb[0] +  ',' + rgb[1] + ',' + rgb[2] + ',1)'
  }

  function rgbToRgba(rgb) {
    var rgba = toRgbaValue(rgb)
    return 'rgba(' + rgba[0] +  ',' + rgba[1] + ',' + rgba[2] + ',' + rgba[3] + ')'
  }

  function toRgbaFromValues(values) {
    var rgb = values.map(Math.round)
    return 'rgba(' + rgb[0] +  ',' + rgb[1] + ',' + rgb[2] + ',' + values[3] + ')'
  }



  //////////
  // ANIM //
  //////////
  function applyValues(obj, steps, curStep) {
    for(var key in steps) {
      if(steps.hasOwnProperty(key)) {
        obj[key] = steps[key][curStep]
      }
    }
  }

  function buildSteps(target, dur, props, easeType) {
    var steps = {}
    for(var key in props) {
      if(props.hasOwnProperty(key)) {
        if(/color/i.test(key)) {
          var propColor = toColorValues(props[key])
          var colorValues = toColorValues(target[key])
            .map(function(c, i) {
              return easy(easeType, c, propColor[i], dur)
            })
          var rs = colorValues[0]
          var gs = colorValues[1]
          var bs = colorValues[2]
          var as = colorValues[3]
          var colors = []
          for(var i = 0, l = rs.length; i < l; i++) {
            colors.push(toRgbaFromValues([rs[i],gs[i],bs[i],as[i]]))	
          }
          steps[key] = colors
        } else if(/^width|^height|^left|^top|^right|^bottom/i.test(key)) {
          steps[key] = easy(easeType, parseInt(target[key], 10), props[key], dur)
            .map(Math.round)
            .map(function(x) { return x + 'px' })
        } else {
          steps[key] = easy(easeType, target[key], props[key], dur)
        }
      }
    }	
    return steps 
  }

  function createRevStepper(inst, steps, cb) {
    return function revStepper() {
      var prop = Object.keys(steps)[0]
      if(steps[prop][inst.curStep] === undefined) {
        inst.curStep = 0
        cancelAnimationFrame(inst.rAF)
        if(typeof cb === 'function') {
          cb.apply(null, [inst])
        }
      } else {
        applyValues(inst.target, steps, inst.curStep)
        inst.curStep --
        inst.rAF = requestAnimationFrame(revStepper)
      }
    }
  }

  function createStepper(inst, steps, cb) {
    return function stepper() {
      var prop = Object.keys(steps)[0]
      if(steps[prop][inst.curStep] === undefined) {
        inst.curStep = inst.steps[prop].length - 1
        cancelAnimationFrame(inst.rAF)
        if(typeof cb === 'function') {
          cb.apply(null, [inst])
        }
      } else {
        applyValues(inst.target, steps, inst.curStep)
        inst.curStep ++
        inst.rAF = requestAnimationFrame(stepper)
      }
    }
  }

  function Anim(target, dur, props) {
    var toProps = {}
    for(var key in props) {
      if(props.hasOwnProperty(key)) {
        if(!/easing|onStart|onUpdate|onComplete/.test(key)) {
          toProps[key] = props[key]
        }
      }
    }
    props.easing = props.easing || 'inOutQuad'
    this.startTime = window.performance.now ?
      (performance.now() + performance.timing.navigationStart) :
      Date.now()
    this.target = target
    //this.target = target instanceof HTMLElement ? target.style : target
    this.onUpdate = props.onUpdate
    this.toProps = toProps
    this.duration = dur * 1000
    this.steps = buildSteps(this.target, this.duration, toProps, props.easing)
    this.curStep = 0
    if(typeof props.onStart === 'function') props.onStart.call(this)
  }

  Anim.prototype = {
    rewind: function(cb) {
      cancelAnimationFrame(this.rAF)
      var _steps = JSON.parse(JSON.stringify(this.steps))
      this.rAF = requestAnimationFrame(createRevStepper(this, _steps, cb))
    },
    play: function(cb) {
      cancelAnimationFrame(this.rAF)
      var _steps = JSON.parse(JSON.stringify(this.steps))
      this.rAF = requestAnimationFrame(createStepper(this, _steps, cb))
    },
    reset: function(cb) {
      this.play(function(inst) {
        inst.curStep = 0
        if(typeof cb === 'function') cb(inst)
      })
    },
    stop: function() {
      cancelAnimationFrame(this.rAF)
    }
  }

  Anim.to = function(target, dur, props) {
    var inst = new Anim(target, dur, props)
    inst.play(props.onComplete)
    return inst
  }

  global.Anim = Anim

}(this))
